@extends('layout.master')

@section('title')
    Halaman Tambah OrangTua
@endsection

@section('content')

<form action="/orangtua" method="POST">
    @csrf
  <div class="form-group">
    <label>Name</label>
    <input type="text" name="name" class="@error('name') is-invalid @enderror form-control" placeholder="Enter name">
  </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Telephone</label>
    <input type="number" name="telfon" class="@error('telfon') is-invalid @enderror form-control" placeholder="Enter telfon">
  </div>
    @error('telfon')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Alamat</label>
    <textarea name="alamat" class="@error('alamat`') is-invalid @enderror form-control" cols="30" rows="10" placeholder="Enter alamat"></textarea>
  </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection