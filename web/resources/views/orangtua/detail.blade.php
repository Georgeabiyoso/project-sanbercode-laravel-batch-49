@extends('layout.master')

@section('title')
    Halaman Detail OrangTua
@endsection

@section('content')

<div class="card border-info " style="max-width: 50rem;">
  <div class="card-header">
    <h2 class="text-dark">{{$orangtua->name}}</h2>
  </div>
  <div class="card-body">
    <h6 class="text-muted font-weight-light">{{$orangtua->telfon}}</h6>
    <p class="card-text text-dark font-weight-normal">{{$orangtua->alamat}}</p>
  </div>
  <div class="card-footer bg-transparent border-info">
    <a href="/orangtua" class="btn btn-sm my-3 btn-secondary">Kembali</a>
  </div>
</div>

@endsection