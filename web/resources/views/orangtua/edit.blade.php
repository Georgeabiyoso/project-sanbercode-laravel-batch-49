@extends('layout.master')

@section('title')
    Halaman Edit OrangTua
@endsection

@section('content')

<form action="/orangtua/{{$orangtua->id}}" method="POST">
    @csrf
    @method('put')
  <div class="form-group">
    <label>Name</label>
    <input type="text" value="{{$orangtua->name}}" name="name" class="@error('name') is-invalid @enderror form-control" placeholder="Enter name">
  </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Telephone</label>
    <input type="number" value="{{$orangtua->telfon}}" name="telfon" class="@error('telfon') is-invalid @enderror form-control" placeholder="Enter telfon">
  </div>
    @error('telfon')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Alamat</label>
    <textarea name="alamat" class="@error('alamat`') is-invalid @enderror form-control" cols="30" rows="10" placeholder="Enter alamat">{{$orangtua->alamat}}</textarea>
  </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection