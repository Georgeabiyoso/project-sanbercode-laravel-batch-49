@extends('layout.master')

@section('title')
    Halaman Tambah Walikelas
@endsection

@section('content')

<form action="/walikelas" method="POST">
    @csrf
  <div class="form-group">
    <label>Walikelas</label>
    <input type="text" name="walikelas" class="@error('walikelas') is-invalid @enderror form-control" placeholder="Enter walikelas">
  </div>
    @error('walikelas')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection