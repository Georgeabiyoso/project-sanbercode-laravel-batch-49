@extends('layout.master')

@section('title')
    Halaman Edit Walikelas
@endsection

@section('content')

<form action="/walikelas/{{$walikelas->id}}" method="POST">
    @csrf
    @method('put')
  <div class="form-group">
    <label>Walikelas</label>
    <input type="text" value="{{$walikelas->walikelas}}" name="walikelas" class="@error('walikelas') is-invalid @enderror form-control" placeholder="Enter walikelas">
  </div>
    @error('walikelas')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection