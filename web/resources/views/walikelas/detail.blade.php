@extends('layout.master')

@section('title')
    Halaman Detail Walikelas
@endsection

@section('content')

<div class="card border-info " style="max-width: 50rem;">
  <div class="card-header">
    <h2 class="text-dark">{{$walikelas->walikelas}}</h2>
  </div>
  <div class="card-footer bg-transparent border-info">
    <a href="/orangtua" class="btn btn-sm my-3 btn-secondary">Kembali</a>
  </div>
</div>

@endsection