@extends('layout.master')

@section('title')
    Halaman Tambah Kelas
@endsection

@section('content')

<form action="/kelas" method="POST">
    @csrf
  <div class="form-group">
    <label>kelas</label>
    <input type="text" name="kelas" class="@error('kelas') is-invalid @enderror form-control" placeholder="Enter Kelas">
  </div>
    @error('kelas')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>tahun</label>
    <input type="number" name="tahun" class="@error('tahun') is-invalid @enderror form-control" placeholder="Enter tahun">
  </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Walikelas</label>
   <select class="form-control" name="walikelas_id" >
    @forelse ($walikelas as $item)
        <option value="{{ $item->id }}"> {{ $item->walikelas }}</option>
    @empty
        <option value="">Tidak ada Walikelas</option>
    @endforelse
   </select>
  </div>
    @error('walikelas')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection