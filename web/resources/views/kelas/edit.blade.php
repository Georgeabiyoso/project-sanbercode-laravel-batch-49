@extends('layout.master')

@section('title')
    Halaman Edit Kelas
@endsection

@section('content')
<form action="/kelas/{{$kelas->id}}" method="POST">
    @csrf
    @method('put')
  <div class="form-group">
    <label>Kelas</label>
    <input type="text" value="{{$kelas->kelas}}" name="kelas" class="@error('kelas') is-invalid @enderror form-control" placeholder="Enter kelas">
  </div>
    @error('kelas')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Tahun</label>
      <input type="text" value="{{$kelas->tahun}}" name="tahun" class="@error('tahun') is-invalid @enderror form-control" placeholder="Enter tahun">
    </div>
      @error('tahun')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
      <label>Wali Kelas </label>
        <select class="form-control" name="walikelas_id">
          <option value="">--Select Walikelas---</option>
          @forelse ($walikelas as $item)
              @if ($item->id === $kelas->walikelas_id)
                  <option value="{{$item->id}}" selected>{{$item->walikelas}}</option>
              @else
              <option value="{{$item->id}}" >{{$item->walikelas}}</option>
              @endif
          @empty
          <option value="">Tidak ada Walikelas</option>
          @endforelse
       </select>
    </div>
      @error('walikelas_id')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection