@extends('layout.master')

@section('title')
    Halaman Tampil Kelas
@endsection

@section('content')

<div class="col-lg-12">
  <div class="card mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 font-weight-bold text-primary">Data kelas</h6>
      <a href="/kelas/create" class="btn btn-primary">[+] Tambah Data</a>
    </div>

    <div class="table-responsive p-3">
      <table class="table align-items-center table-flush table-striped" id="dataTable">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">kelas</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
        @forelse ($kelas as $key => $item)
          <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$item->kelas}}</td>
            <td>
              <form action="/kelas/{{$item->id}}" method="POST">
                  @csrf
                  @method('delete')
                  <a href="/kelas/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                  <a href="/kelas/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-sm btn-danger">
              </form>
            </td>
          </tr>
          @empty
          <tr>
              <td>Tidak Ada Data Kelas</td>
          </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>

@endsection