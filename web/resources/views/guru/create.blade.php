@extends('layout.master')

@section('title')
    Halaman Tambah Guru
@endsection

@section('content')

<form action="/guru" method="POST">
    @csrf
  <div class="form-group">
    <label>nama</label>
    <input type="text" name="name" class="@error('name') is-invalid @enderror form-control" placeholder="Enter nama">
  </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>alamat</label>
    <input type="text" name="alamat" class="@error('alamat') is-invalid @enderror form-control" placeholder="Enter alamat">
  </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
    <label>Telfon</label>
    <input type="text" name="telfon" class="@error('telfon') is-invalid @enderror form-control" placeholder="Enter telfon">
  </div>
    @error('telfon')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Walikelas</label>
   <select class="form-control" name="walikelas_id" >
    @forelse ($walikelas as $item)
        <option value="{{ $item->id }}"> {{ $item->walikelas }}</option>
    @empty
        <option value="">Tidak ada Walikelas</option>
    @endforelse
   </select>
  </div>
    @error('walikelas')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection