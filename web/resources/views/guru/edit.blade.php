@extends('layout.master')

@section('title')
    Halaman Edit Guru
@endsection

@section('content')
<form action="/guru/{{$guru->id}}" method="POST">
    @csrf
    @method('put')
  <div class="form-group">
    <label>Name</label>
    <input type="text" value="{{$guru->name}}" name="name" class="@error('name') is-invalid @enderror form-control" placeholder="Enter name">
  </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Alamat</label>
      <input type="text" value="{{$guru->alamat}}" name="alamat" class="@error('alamat') is-invalid @enderror form-control" placeholder="Enter alamat">
    </div>
      @error('alamat')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Telfon</label>
        <input type="text" value="{{$guru->telfon}}" name="telfon" class="@error('telfon') is-invalid @enderror form-control" placeholder="Enter telfon">
      </div>
        @error('telfon')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror


      <div class="form-group">
        <label>Walikelas</label>
          <select class="form-control" name="walikelas_id">
            <option value="">--Select Walikelas---</option>
            @forelse ($walikelas as $item)
                @if ($item->id === $guru->walikelas_id)
                    <option value="{{$item->id}}" selected>{{$item->walikelas}}</option>
                @else
                <option value="{{$item->id}}" >{{$item->walikelas}}</option>
                @endif
            @empty
            <option value="">Tidak ada walikelas</option>
            @endforelse
         </select>
      </div>
      @error('walikelas_id')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection