@extends('layout.master')

@section('title')
    Halaman Edit Siswa
@endsection

@section('content')
<form action="/siswa/{{$siswa->id}}" method="POST">
    @csrf
    @method('put')
  <div class="form-group">
    <label>Name</label>
    <input type="text" value="{{$siswa->name}}" name="name" class="@error('name') is-invalid @enderror form-control" placeholder="Enter name">
  </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Tanggal Lahir</label>
    <input type="date" value="{{$siswa->tanggallahir}}" name="tanggallahir" class="@error('tanggallahir') is-invalid @enderror form-control">
  </div>
    @error('tanggallahir')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label for="kelamin">Jenis Kelamin</label>
      <select name="kelamin" id="kelamin" class="form-control @error('kelamin') is-invalid @enderror">
          <option value="Laki-Laki" {{ $siswa->kelamin == 'Laki-Laki' ? 'selected' : '' }}>Laki-Laki</option>
          <option value="Perempuan" {{ $siswa->kelamin == 'Perempuan' ? 'selected' : '' }}>Perempuan</option>
      </select>
  </div>
      @error('kelamin')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>No Telfon</label>
        <input type="number" value="{{$siswa->telfon}}" name="telfon" class="@error('telfon') is-invalid @enderror form-control">
      </div>
        @error('telfon')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

    <div class="form-group">
    <label>Alamat</label>
    <textarea name="alamat"  class="@error('alamat') is-invalid @enderror form-control" cols="30" rows="10" placeholder="Enter alamat">{{$siswa->alamat}}</textarea>
  </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Kelas</label>
        <select class="form-control" name="kelas_id">
          <option value="">--Select Kelas---</option>
          @forelse ($kelas as $item)
              @if ($item->id === $siswa->kelas_id)
                  <option value="{{$item->id}}" selected>{{$item->kelas}}</option>
              @else
              <option value="{{$item->id}}" >{{$item->kelas}}</option>
              @endif
          @empty
          <option value="">Tidak ada kelas</option>
          @endforelse
       </select>
    </div>
      @error('kelas')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Orangtua</label>
          <select class="form-control" name="orangtua_id">
            <option value="">--Select Orangtua---</option>
            @forelse ($orangtua as $item)
                @if ($item->id === $siswa->orangtua_id)
                    <option value="{{$item->id}}" selected>{{$item->name}}</option>
                @else
                <option value="{{$item->id}}" >{{$item->name}}</option>
                @endif
            @empty
            <option value="">Tidak ada orangtua</option>
            @endforelse
         </select>
      </div>
        @error('orangtua_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection