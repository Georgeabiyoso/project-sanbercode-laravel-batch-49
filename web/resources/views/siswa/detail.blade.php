@extends('layout.master')

@section('title')
    Halaman Detail Siswa
@endsection

@section('content')

<div class="card border-info " style="max-width: 50rem;">
  <div class="card-header">
    <h2 class="text-dark">{{$siswa->name}}</h2>
  </div>
  <div class="card-body">
    <h6 class="text-muted font-weight-light">Tanggal Lahir : {{$siswa->tanggallahir}}</h6>
    <p class="card-text text-dark font-weight-normal">Jenis Kelamin : {{$siswa->kelamin}}</p>
    <p class="card-text text-dark font-weight-normal">Nomer Telfon : {{$siswa->telfon}}</p>
    <p class="card-text text-dark font-weight-normal">Alamat Siswa : {{$siswa->alamat}}</p>
    <p class="card-text text-dark font-weight-normal">Kelas : {{$siswa->kelas->kelas}}</p>
    <p class="card-text text-dark font-weight-normal">nama Orang Tua : {{$siswa->orangtua->name}}</p>

  </div>
  <div class="card-footer bg-transparent border-info">
    <a href="/siswa" class="btn btn-sm my-3 btn-secondary">Kembali</a>
  </div>
</div>

@endsection