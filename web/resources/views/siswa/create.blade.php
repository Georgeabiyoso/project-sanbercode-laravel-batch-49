@extends('layout.master')

@section('title')
    Halaman Tambah Siswa
@endsection

@section('content')

<form action="/siswa" method="POST">
    @csrf
  <div class="form-group">
    <label>Name</label>
    <input type="text" name="name" class="@error('siswa') is-invalid @enderror form-control" placeholder="Enter name">
  </div>
    @error('siswa')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Tanggal Lahir</label>
    <input type="date" name="tanggallahir" class="@error('tanggallahir') is-invalid @enderror form-control" >
  </div>
    @error('tanggallahir')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Jenis Kelamin</label>
      <select type="text"name="kelamin" class="@error('kelamin') is-invalid @enderror form-control" >
        <option value="">--Select Jenis Kelamin--</option>
      <option>Laki-Laki</option>
      <option>Perempuan</option>
    </select>
    </div>
    @error('kelamin')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    
    <div class="form-group">
      <label>Telfon</label>
      <input type="number" name="telfon" class="@error('telfon') is-invalid @enderror form-control" cols="30" rows="10" placeholder="Enter Telfon">
    </div>
      @error('telfon')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      
    <div class="form-group">
      <label>alamat</label>
      <textarea name="alamat" class="@error('alamat') is-invalid @enderror form-control" cols="30" rows="10" placeholder="Enter alamat"></textarea>
    </div>
      @error('alamat')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  
      <div class="form-group">
        <label>Kelas</label>
        <select class="form-control" name="kelas_id">
          <option value="">--Select Kelas--</option>
          @forelse ($kelas as $item)
            <option value="{{$item->id}}">{{$item->kelas}}</option>
          @empty
            <option value="">Tidak ada kelas</option>
          @endforelse
        </select> 
        </div>
      @error('kelas_id')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Orangtua</label>
        <select class="form-control" name="orangtua_id">
          <option value="">--Select Orangtua--</option>
          @forelse ($orangtua as $item)
            <option value="{{$item->id}}">{{$item->name}}</option>
          @empty
            <option value="">Tidak ada orangtua</option>
          @endforelse
        </select> 
        </div>
        @error('orangtua_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection