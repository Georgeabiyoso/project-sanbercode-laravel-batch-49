@extends('layout.master')

@section('title')
    Halaman Tambah MataPelajaran
@endsection

@section('content')

<form action="/matapelajaran" method="POST">
    @csrf
  <div class="form-group">
    <label>Name</label>
    <input type="text" name="name" class="@error('name') is-invalid @enderror form-control" placeholder="Enter name">
  </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Siswa</label>
      <select class="form-control" name="siswa_id">
        <option value="">--Select Siswa--</option>
        @forelse ($siswa as $item)
          <option value="{{$item->id}}">{{$item->name}}</option>
        @empty
          <option value="">Tidak ada Nama</option>
        @endforelse
      </select> 
      </div>
      @error('siswa_id')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Guru</label>
        <select class="form-control" name="guru_id">
          <option value="">--Select Guru--</option>
          @forelse ($guru as $item)
            <option value="{{$item->id}}">{{$item->name}}</option>
          @empty
            <option value="">Tidak ada Guru</option>
          @endforelse
        </select> 
        </div>
        @error('orangtua_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection