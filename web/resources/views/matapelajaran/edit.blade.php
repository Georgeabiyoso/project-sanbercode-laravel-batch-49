@extends('layout.master')

@section('title')
    Halaman Edit Mata Pelajaran
@endsection

@section('content')
<form action="/matapelajaran/{{$matapelajaran->id}}" method="POST">
    @csrf
    @method('put')
  <div class="form-group">
    <label>Name</label>
    <input type="text" value="{{$matapelajaran->name}}" name="name" class="@error('name') is-invalid @enderror form-control" placeholder="Enter name">
  </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Nama Siswa</label>
        <select class="form-control" name="siswa_id">
          <option value="">--Select Siswa---</option>
          @forelse ($siswa as $item)
              @if ($item->id === $matapelajaran->siswa_id)
                  <option value="{{$item->id}}" selected>{{$item->name}}</option>
              @else
              <option value="{{$item->id}}" >{{$item->name}}</option>
              @endif
          @empty
          <option value="">Tidak ada siswa</option>
          @endforelse
       </select>
    </div>
      @error('siswa_id')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Nama Guru</label>
          <select class="form-control" name="guru_id">
            <option value="">--Select Guru---</option>
            @forelse ($guru as $item)
                @if ($item->id === $matapelajaran->guru_id)
                    <option value="{{$item->id}}" selected>{{$item->name}}</option>
                @else
                <option value="{{$item->id}}" >{{$item->name}}</option>
                @endif
            @empty
            <option value="">Tidak ada Guru</option>
            @endforelse
         </select>
      </div>
        @error('guru_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection