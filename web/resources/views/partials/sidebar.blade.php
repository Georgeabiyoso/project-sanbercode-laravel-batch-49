<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
          <img src="{{asset('./template/img/logo/logo.png')}}">
        </div>
        <div class="sidebar-brand-text mx-3">Manajemen Sekolah</div>
      </a>
      <hr class="sidebar-divider my-0">
      <li class="nav-item">
        <a class="nav-link" href="/">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Features
      </div>

      <li class="nav-item">
        <a class="nav-link" href="/guru">
          <i class="fas fa-fw fa-id-card"></i>
          <span>Guru</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="/kelas">
          <i class="fas fa-fw fa-chalkboard"></i>
          <span>Kelas</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="/matapelajaran">
          <i class="fas fa-fw fa-book"></i>
          <span>Mata Pelajaran</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="/orangtua">
          <i class="fas fa-fw fa-user-friends"></i>
          <span>Orang Tua</span>
        </a>
      </li>
    
      <li class="nav-item">
        <a class="nav-link" href="/siswa">
          <i class="fas fa-fw fa-user-graduate"></i>
          <span>Siswa</span>
        </a>
      </li>

      <li class="nav-item bg-danger">
        <a class="nav-link" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
            Logout
        </a>
      </li>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
  </ul>

  @push('js')
      <script>
    document.getElementById('logout-form').addEventListener('submit', function () {
        // Setelah logout berhasil, arahkan pengguna ke halaman login
        window.location.href = '/login';
    });
</script>
s
  @endpush