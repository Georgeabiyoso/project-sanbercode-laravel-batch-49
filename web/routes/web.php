<?php

use App\Http\Controllers\GuruController;
use App\Http\Controllers\KelasController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\OrangtuaController;
use App\Http\Controllers\WalikelasController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\MatapelajaranController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'dashboard']);


//CRUD ORANG TUA
//CREATE
Route::get('/orangtua/create', [OrangtuaController::class, 'create']);
//route ke database
Route::post('/orangtua', [OrangtuaController::class, 'store']);
//READ
Route::get('/orangtua', [OrangtuaController::class, 'index']);
//route untuk detail data
Route::get('/orangtua/{id}', [OrangtuaController::class, 'show']);
//UPDATE
Route::get('/orangtua/{id}/edit', [OrangtuaController::class, 'edit']);
//route update database berdasarkan id
Route::put('/orangtua/{id}', [OrangtuaController::class, 'update']);
//DELETE
Route::delete('/orangtua/{id}', [OrangtuaController::class, 'destroy']);


//CRUD WALIKELAS
//CREATE
Route::get('/walikelas/create', [WalikelasController::class, 'create']);
//route ke database
Route::post('/walikelas', [WalikelasController::class, 'store']);
//READ
Route::get('/walikelas', [WalikelasController::class, 'index']);
//route untuk detail data
Route::get('/walikelas/{id}', [WalikelasController::class, 'show']);
//UPDATE
Route::get('/walikelas/{id}/edit', [WalikelasController::class, 'edit']);
//route update database berdasarkan id
Route::put('/walikelas/{id}', [WalikelasController::class, 'update']);
//DELETE
Route::delete('/walikelas/{id}', [WalikelasController::class, 'destroy']);

// CRUD KELAS
Route::get('/kelas/create', [KelasController::class, 'create']);
//route ke database
Route::post('/kelas', [KelasController::class, 'store']);
//READ
Route::get('/kelas', [KelasController::class, 'index']);
//route untuk detail data
Route::get('/kelas/{id}', [KelasController::class, 'show']);
//UPDATE
Route::get('/kelas/{id}/edit', [KelasController::class, 'edit']);
//route update database berdasarkan id
Route::put('/kelas/{id}', [KelasController::class, 'update']);
//DELETE
Route::delete('/kelas/{id}', [KelasController::class, 'destroy']);

// CRUD GURU
// CREATE
Route::get('/guru/create', [GuruController::class, 'create']);
//route ke database
Route::post('/guru', [GuruController::class, 'store']);
//READ
Route::get('/guru', [GuruController::class, 'index']);
//route untuk detail data
Route::get('/guru/{id}', [GuruController::class, 'show']);
//UPDATE
Route::get('/guru/{id}/edit', [GuruController::class, 'edit']);
//route update database berdasarkan id
Route::put('/guru/{id}', [GuruController::class, 'update']);
//DELETE
Route::delete('/guru/{id}', [GuruController::class, 'destroy']);

//CRUD Siswa
//CREATE
Route::get('/siswa/create', [SiswaController::class, 'create']);
//route ke database
Route::post('/siswa', [SiswaController::class, 'store']);
//READ
Route::get('/siswa', [SiswaController::class, 'index']);
//route untuk detail data
Route::get('/siswa/{id}', [SiswaController::class, 'show']);
//UPDATE
Route::get('/siswa/{id}/edit', [SiswaController::class, 'edit']);
//route update database berdasarkan id
Route::put('/siswa/{id}', [SiswaController::class, 'update']);
//DELETE
Route::delete('/siswa/{id}', [SiswaController::class, 'destroy']);

//CRUD Matapelajaran
//CREATE
Route::get('/matapelajaran/create', [MatapelajaranController::class, 'create']);
//route ke database
Route::post('/matapelajaran', [MatapelajaranController::class, 'store']);
//READ
Route::get('/matapelajaran', [MatapelajaranController::class, 'index']);
//route untuk detail data
Route::get('/matapelajaran/{id}', [MatapelajaranController::class, 'show']);
//UPDATE
Route::get('/matapelajaran/{id}/edit', [MatapelajaranController::class, 'edit']);
//route update database berdasarkan id
Route::put('/matapelajaran/{id}', [MatapelajaranController::class, 'update']);
//DELETE
Route::delete('/matapelajaran/{id}', [MatapelajaranController::class, 'destroy']);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// routes/web.php