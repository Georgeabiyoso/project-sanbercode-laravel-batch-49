<?php

namespace App\Http\Controllers;
use App\Models\Kelas;
use App\Models\Walikelas;


use Illuminate\Http\Request;
use PhpParser\Node\Expr\New_;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $kelas=Kelas::all();
      return view('kelas.read', ['kelas'=>$kelas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $walikelas=Walikelas::all();
        return view ('kelas.create', ['walikelas'=>$walikelas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kelas' => 'required|max:100',
            'tahun' => 'required|max:11|min:3',
            'walikelas_id' => 'required',
        ]);
$kelas= new Kelas;
$kelas->kelas=$request->input('kelas');
$kelas->tahun=$request->input('tahun');
$kelas->walikelas_id=$request->input('walikelas_id');
$kelas->save();
return redirect('/kelas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kelas = Kelas::find($id);
        return view('kelas.detail', ['kelas' => $kelas]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelas= Kelas::find($id);
        $walikelas= Walikelas::all();
        return view('kelas.edit' ,['kelas'=> $kelas,'walikelas'=>$walikelas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $request->validate([
            'kelas' => 'required|max:100',
            'tahun' => 'required|max:11|min:3',
            'walikelas_id' => 'required',
        ]);
$kelas= Kelas::find($id);
$kelas->kelas=$request->input('kelas');
$kelas->tahun=$request->input('tahun');
$kelas->walikelas_id=$request->input('walikelas_id');
$kelas->save();
return redirect('/kelas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelas =Kelas::find($id);
        $kelas->delete();
        return redirect('/kelas');

    }
}