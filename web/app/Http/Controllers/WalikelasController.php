<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WalikelasController extends Controller
{
    public function create()
    {
        return view('walikelas.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'walikelas' => 'required|max:255|min:3'
        ]);

        //tambah ke database table walikelas
        DB::table('walikelas')->insert([
            'walikelas' => $request->input('walikelas')
        ]);

        return redirect('/walikelas');
    }

    public function index()
    {
        $walikelas = DB::table('walikelas')->get();

        return view('walikelas.read', ['walikelas' => $walikelas]);
    }

    public function show($id)
    {
        $walikelas = DB::table('walikelas')->find($id);

        return view('walikelas.detail', ['walikelas' => $walikelas]);
    }

    public function edit($id)
    {
        $walikelas = DB::table('walikelas')->find($id);

        return view('walikelas.edit', ['walikelas' => $walikelas]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'walikelas' => 'required|max:255|min:3',
        ]);

        DB::table('walikelas')
              ->where('id', $id)
              ->update(
                [
                    'walikelas' => $request->input('walikelas')
                ]
            );
        return redirect('/walikelas');
    }

    public function destroy($id)
    {
        DB::table('walikelas')->where('id', $id)->delete();

        return redirect('/walikelas');
    }
}
