<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrangtuaController extends Controller
{
    public function create()
    {
        return view('orangtua.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'telfon' => 'required',
            'alamat' => 'required',
        ]);

        //tambah ke database table orangtua
        DB::table('orangtua')->insert([
            'name' => $request->input('name'),
            'telfon' => $request->input('telfon'),
            'alamat' => $request->input('alamat')
        ]);

        return redirect('/orangtua');
    }

    public function index()
    {
        $orangtua = DB::table('orangtua')->get();

        return view('orangtua.read', ['orangtua' => $orangtua]);
    }

    public function show($id)
    {
        $orangtua = DB::table('orangtua')->find($id);

        return view('orangtua.detail', ['orangtua' => $orangtua]);
    }

    public function edit($id)
    {
        $orangtua = DB::table('orangtua')->find($id);

        return view('orangtua.edit', ['orangtua' => $orangtua]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'telfon' => 'required',
            'alamat' => 'required',
        ]);

        DB::table('orangtua')
              ->where('id', $id)
              ->update(
                [
                    'name' => $request->input('name'),
                    'telfon' => $request->input('telfon'),
                    'alamat' => $request->input('alamat')
                ]
            );
        return redirect('/orangtua');
    }

    public function destroy($id)
    {
        DB::table('orangtua')->where('id', $id)->delete();

        return redirect('/orangtua');
    }
}