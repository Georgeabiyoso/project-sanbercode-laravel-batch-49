<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siswa;
use App\Models\Guru;
use App\Models\Matapelajaran;

class MatapelajaranController extends Controller
{
    public function create()
    {

        $guru = Guru::all();
        $siswa = Siswa::all();
        return view('matapelajaran.create', ['guru'=> $guru, 'siswa'=> $siswa]);

    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'siswa_id' => 'required',
            'guru_id' => 'required',
        ]);

        $matapelajaran= new Matapelajaran;
        $matapelajaran->name=$request->input('name');
        $matapelajaran->siswa_id=$request->input('siswa_id');
        $matapelajaran->guru_id=$request->input('guru_id');
        $matapelajaran->save();
        return redirect('/matapelajaran');
    }

    public function index()
    {
        $matapelajaran = Matapelajaran::all();
        return view('matapelajaran.read', ['matapelajaran' => $matapelajaran]);
    }

    public function show($id)
    {
        $matapelajaran = Matapelajaran::find($id);
        return view('matapelajaran.detail', ['matapelajaran' => $matapelajaran]);
    }

    public function edit($id)
    {
        $matapelajaran= Matapelajaran::find($id);
        $siswa= Siswa::all();
        $guru= Guru::all();
        return view('matapelajaran.edit' ,['matapelajaran'=> $matapelajaran ,'siswa'=>$siswa,'guru'=>$guru]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'siswa_id' => 'required',
            'guru_id' => 'required',
        ]);

        $matapelajaran= Matapelajaran::find($id);
        $matapelajaran->name=$request->input('name');
        $matapelajaran->siswa_id=$request->input('siswa_id');
        $matapelajaran->guru_id=$request->input('guru_id');
        $matapelajaran->save();
        return redirect('/matapelajaran');
    }

    public function destroy($id)
    {
        $matapelajaran = Matapelajaran::find($id);
        $matapelajaran->delete();

        return redirect('/matapelajaran');
    }
}
