<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\Walikelas;
use Illuminate\Http\Request;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $guru = Guru::all();
        return view('guru.read', ['guru' => $guru]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $walikelas=Walikelas::all();
        return view ('guru.create', ['walikelas'=>$walikelas]);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'name' => 'required|max:100',
            'alamat' => 'required',
            'telfon' => 'required|max:11|min:3',
            'walikelas_id' => 'required',
        ]);
            $guru= new Guru;
            $guru->name=$request->input('name');
            $guru->alamat=$request->input('alamat');
            $guru->telfon=$request->input('telfon');
            $guru->walikelas_id=$request->input('walikelas_id');
            
            $guru->save();
            return redirect('/guru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          $guru = Guru::find($id);
        return view('guru.detail', ['guru' => $guru]);
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $guru= Guru::find($id);
        $walikelas= Walikelas::all();
        return view('guru.edit' ,['guru'=> $guru,'walikelas'=>$walikelas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'name' => 'required|max:100',
            'alamat' => 'required',
            'telfon' => 'required|max:11|min:3',
            'walikelas_id' => 'required',
        ]);
        $guru= Guru::find($id);
        $guru->name=$request->input('name');
        $guru->alamat=$request->input('alamat');
        $guru->telfon=$request->input('telfon');
        $guru->walikelas_id=$request->input('walikelas_id');
        $guru->save();
        return redirect('/guru');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $guru = Guru::find($id);
        $guru->delete();
        return redirect('/guru');

    }
}