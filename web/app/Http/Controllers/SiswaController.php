<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kelas;
use App\Models\Orangtua;
use App\Models\Siswa;


class SiswaController extends Controller
{
    public function create()
    {

        $orangtua = Orangtua::all();
        $kelas = Kelas::all();

        return view('siswa.create', ['kelas'=> $kelas, 'orangtua'=> $orangtua]);

    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'tanggallahir' => 'required',
            'kelamin' => 'required',
            'telfon' => 'required|max:11|min:3',
            'alamat' => 'required',
            'kelas_id' => 'required',
            'orangtua_id' => 'required',
        ]);

        //tambah ke database table siswa
        $siswa= new Siswa;
        $siswa->name=$request->input('name');
        $siswa->tanggallahir=$request->input('tanggallahir');
        $siswa->kelamin=$request->input('kelamin');
        $siswa->telfon=$request->input('telfon');
        $siswa->alamat=$request->input('alamat');
        $siswa->kelas_id=$request->input('kelas_id');
        $siswa->orangtua_id=$request->input('orangtua_id');
        $siswa->save();

        return redirect('/siswa');
    }

    public function index()
    {
        $siswa = Siswa::all();
        return view('siswa.read', ['siswa' => $siswa]);
    }

    public function show($id)
    {
        $siswa = Siswa::find($id);
        return view('siswa.detail', ['siswa' => $siswa]);
    }

    public function edit($id)
    {
        $siswa = Siswa::find($id);
        $kelas = Kelas::all();
        $orangtua = Orangtua::all();
        return view('siswa.edit' ,['siswa' => $siswa ,'kelas' => $kelas,'orangtua' => $orangtua]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'tanggallahir' => 'required',
            'kelamin' => 'required',
            'telfon' => 'required|max:11|min:3',
            'alamat' => 'required',
            'kelas_id' => 'required',
            'orangtua_id' => 'required',
        ]);

        $siswa= new Siswa;
        $siswa->name=$request->input('name');
        $siswa->tanggallahir=$request->input('tanggallahir');
        $siswa->kelamin=$request->input('kelamin');
        $siswa->telfon=$request->input('telfon');
        $siswa->alamat=$request->input('alamat');
        $siswa->kelas_id=$request->input('kelas_id');
        $siswa->orangtua_id=$request->input('orangtua_id');
        $siswa->save();
        return redirect('/siswa');
    }

    public function destroy($id)
    {
        $siswa = Siswa::find($id);
        $siswa->delete();
        return redirect('/siswa');
    }
}
