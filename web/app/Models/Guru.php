<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    use HasFactory;
    
    protected $table = 'guru';
    
    protected $fillable = ['name', 'alamat','telfon', 'walikelas_id'];
    public function walikelas()
    {
        return $this->belongsTo(Walikelas::class, 'walikelas_id');
    }
}