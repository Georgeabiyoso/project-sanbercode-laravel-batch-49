<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matapelajaran extends Model
{
    use HasFactory;
    protected $table = 'matapelajaran';

    protected $fillable = ['name', 'siswa_id', 'guru_id'];
    public function siswa()
    {
        return $this->belongsTo(siswa::class, 'siswa_id');
    }
    public function guru()
    {
        return $this->belongsTo(guru::class, 'guru_id');
    }
}
