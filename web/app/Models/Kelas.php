<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    use HasFactory;
    protected $table = 'kelas';
    
    protected $fillable = ['kelas', 'tahun', 'walikelas_id'];
    public function walikelas()
    {
        return $this->belongsTo(Walikelas::class, 'walikelas_id');
    }
}