<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class siswa extends Model
{
    use HasFactory;

    protected $table = 'siswa';

    protected $fillable = ['name', 'tanggallahir', 'kelamin', 'telfon','alamat', 'kelas_id', 'orangtua_id'];

    public function kelas()
    {
        return $this->belongsTo(kelas::class, 'kelas_id');
    }
    public function orangtua()
    {
        return $this->belongsTo(orangtua::class, 'orangtua_id');
    }
}
