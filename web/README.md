# Final Project Sanbercode Laravel Batch 49

## Kelompok 3

## Anggota Kelompok

1. George Isaiah Abiyoso
2. Dimmas Putra Drajatulloh
3. Mochamad Wahyu

## Tema Project

Sistem Manajemen Sekolah

## ERD
![Img 1](ERD.png)


## Link Video dan Deploy

1. Link Demo Aplikasi : ttps://drive.google.com/file/d/1uBMpR0kcwtUA7R-SECJch2ScS98cArfJ/view?usp=sharing
2. Link Deploy : https://rskm2.karyamedika.com/web/public/login
